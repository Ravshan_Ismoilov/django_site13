from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey
from phone_field import PhoneField
from django.utils import timezone
 
app_name = 'index'

class Menu(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=255, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, \
    	null=True, blank=True, related_name='children')

    def __str__(self):
    	return self.name

    def get_absolute_url(self):
    	return self.slug

    class MPTTMeta:
        order_insertion_by = ['name']

class Teachers(models.Model):
    STATUS_CHOICES = (
        ('faol', 'Faol'),
        ('nofaol', 'Nofaol'),
    )
    TOIFA_CHOICES = (
        ('n', 'Toifaga ega emas'),
        ('m', 'Mutaxasis'),
        ('ikki', 'Ikkinchi toifa'),
        ('bir', 'Birinchi toifa'),
        ('oliy', 'Oliy toifa'),
    )
    MALUMOT_CHOICES = (
        ('oliy', 'Oliy'),
        ('urta', 'O\'rta maxsus'),
    )
    name = models.CharField(max_length=255)
    full_name = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    wasborn = models.DateField(auto_now=False)
    phone_number = PhoneField(blank=True, help_text='Contact phone number')
    email = models.EmailField(blank=True)
    image = models.ImageField(upload_to='teachers/%Y/%m/%d', blank=True, default='blog/no_image.png')
    malumoti = models.CharField(max_length=10, choices=MALUMOT_CHOICES, default='urta')
    toifa = models.CharField(max_length=10, choices=TOIFA_CHOICES, default='n')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='nofaol')
    
    def __str__(self):
        return self.name
        