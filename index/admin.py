from django.contrib import admin
# from mptt.admin import MPTTModelAdmin
from mptt.admin import DraggableMPTTAdmin
from django.utils.safestring import mark_safe
from .models import Menu, Teachers


# class CategoryAdmin(admin.ModelAdmin):
# 	save_as = True

# admin.site.register(Menu, MPTTModelAdmin)
admin.site.register(
    Menu,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    ),
    prepopulated_fields = {'slug': ('name',)},
)
class TeachersAdmin(admin.ModelAdmin):
    list_display = ('name', 'full_name', 'email', 'phone_number','malumoti', 'toifa', 'status', 'get_photo')
    list_filter = ('name', 'full_name', 'status', 'created')
    search_fields = ('name', 'full_name', 'phone_number', 'malumoti', 'toifa')
    readonly_fields = ('created', 'updated', 'get_photo')
    # fields = ('name', 'full_name', 'email', 'phone_number','malumoti', 'toifa', 'image', 'status', 'get_photo')
    save_as = True
    save_on_top = True

    def get_photo(self, obj):
        return mark_safe(f'<img src="{obj.image.url}" width="50">')

    get_photo.short_description = "Photo"
    

admin.site.register(Teachers, TeachersAdmin)