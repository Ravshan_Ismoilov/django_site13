# Generated by Django 3.0.5 on 2021-03-10 11:50

from django.db import migrations, models
import django.db.models.deletion
import mptt.fields
import phone_field.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Teachers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('full_name', models.CharField(max_length=255)),
                ('city', models.CharField(max_length=255)),
                ('wasborn', models.DateField()),
                ('phone_number', phone_field.models.PhoneField(blank=True, help_text='Contact phone number', max_length=31)),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('image', models.ImageField(blank=True, default='blog/no_image.png', upload_to='teachers/%Y/%m/%d')),
                ('malumoti', models.CharField(choices=[('oliy', 'Oliy'), ('urta', "O'rta maxsus")], default='urta', max_length=10)),
                ('toifa', models.CharField(choices=[('n', 'Toifaga ega emas'), ('m', 'Mutaxasis'), ('bir', 'Birinchi toifa'), ('ikki', 'Ikkinchi toifa'), ('oliy', 'Oliy toifa')], default='n', max_length=10)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('faol', 'Faol'), ('nofaol', 'Nofaol')], default='nofaol', max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('slug', models.SlugField(max_length=255, unique=True)),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='index.Menu')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
